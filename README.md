# DAForge_IA -  Artificial Intelligence in Delphi -  #

  Porting of this because it is difficult to find good implementations of Artificial Intelligence in delphi

### NOTE ###

* Porting of a part of the framework [AForge.NET](http://www.aforgenet.com) in Delphi.
* Porting modules: 
     * AForge.Neuro - Neural networks computation library;
     *  AForge.Genetic - evolution programming library; 
     *  AForge.MachineLearning -  machine learning library; 


### P.S ###
* For Compiling the test of Reinforcement learning is required TMS-TADVStringGrid
* welcome collaborative testing and improvement of source code.
I have little free time.